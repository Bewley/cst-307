				.data
prompt1: 	.asciiz "Enter the 1st integer: "
prompt2: 	.asciiz "Enter the 2nd integer: "
response: 	.asciiz "\nThe larger of the two numbers you entered was: "

				.globl main
				.text
main: 		li $v0, 4 				#Syscall for print string
				la $a0, prompt1 	#Load prompt1 into $a0
				syscall					#Execute print string
			
				li $v0, 5				#Syscall for read integer
				syscall					#Execute read int into $v0
			
				move $t0, $v0		#Move int from $v0 to $t0
			
				li $v0, 4 				#Syscall for print string
				la $a0, prompt2 	#Load prompt2 into $a0
				syscall
			
				li $v0, 5				#Syscall for read integer
				syscall					#Execute read int into $v0
			
				move $t1, $v0		#Move int from $v0 to $t0
				
				li $v0, 4 				#Syscall for print string
				la $a0, response 	#Load response into $a0
				syscall					#Execute print string
				
				sub $t2, $t0, $t1	#Store ($t0 - $t1) into $t2
				
				bgez $t2, first		#If difference < 0, branch to first label

				b second				#Else branch to second label

first:			li $v0, 1				#Syscall for print int
				move $a0, $t0		#Move first number to $a0
				syscall					#Excecute print int
				
				b done
				
second: 	li $v0, 1				#Syscall for print int
				move $a0, $t1		#Move first number to $a0
				syscall					#Excecute print int
				
				b done
				
done:		li $v0, 10				#Syscall for terminate program
				syscall					#Excecute termination