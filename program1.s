			.data
prompt1: .asciiz "Please enter first integer to add: "
prompt2: .asciiz "Please enter next integer to add: "
response: .asciiz "The sum of your two integers is: "

			.globl main
			.text
main:   	li $v0, 4				#Syscall for print string
			la $a0, prompt1		#Move prompt1 into $a0 register for printing
			syscall					#Exectute print string
			
			li $v0, 5				#Syscall for read integer
			syscall					#Execute read int into $v0
			
			move $t0, $v0		#Move int from $v0 to $t0
			
			li $v0, 4				#Syscall for print string
			la $a0, prompt2		#Move prompt2 into $a0 register for printing
			syscall					#Exectute print string
			
			li $v0, 5				#Syscall for read integer
			syscall					#Execute read int into $v0
			
			move $t1, $v0		#Move int from $v0 to $t1
			
			add $t2, $t0, $t1	#Add $t0 and $t1, place in $t2
			
			li $v0, 4				#Syscall for print string
			la $a0, response	#Move response into $a0 register for printing
			syscall					#Exectute print string
			
			li $v0, 1				#Syscall for print int
			move $a0, $t2		#Move sum into $a0 for printing
			syscall					#Excecute print int to print result
			
			li $v0, 10				#Syscall for terminate program
			syscall					#Excecute termination